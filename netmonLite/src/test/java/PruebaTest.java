

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.netmon.model.Dispositivo;
import com.netmon.model.Prueba;

public class PruebaTest {

	Dispositivo dispositivo = new Dispositivo("disp1", "127.0.0.1", "Alerta");
	List<Prueba> pruebas = new ArrayList<>();
	Prueba prueba = new Prueba("Ping", "127.0.0.1", "2000");

	@Test
	public void Prueba() {
		assertNotNull(prueba);
		assertNotNull(new Prueba());
	}

	@Test
	public void nombre() {
		prueba.setNombre("Port");
		assertTrue(prueba.getNombre().equals("Port"));
	}

	@Test
	public void frecuencia() {
		prueba.setFrecuencia("5000");
		assertTrue(prueba.getFrecuencia().equals("5000"));
	}

	@Test
	public void resultado() {
		prueba.setResultado("False");
		assertTrue(prueba.getResultado().equals("False"));
	}

	@Test
	public void ejecucion() {
		prueba.setEjecucion(true);
		assertTrue(prueba.isEjecucion());
	}

	@Test
	public void parametros() {
		prueba.setParametros("80 2");
		assertTrue(prueba.getParametros().equals("80 2"));
	}

	@Test
	public void ejecutarPrueba() throws Exception {
		pruebas.add(prueba);
		dispositivo.setPruebas(pruebas);

		prueba.ejecutarPrueba("./src/test/resources/", dispositivo);
		
	}

}