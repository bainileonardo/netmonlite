

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.netmon.model.Ubicacion;

public class UbicacionTest {

	Ubicacion ubicacion = new Ubicacion();

	@Test
	public void Ubicacion() {
		assertNotNull(ubicacion);
	}

	@Test
	public void x() {
		ubicacion.setX(20);
		assertEquals(20, ubicacion.getX().intValue());
	}

	@Test
	public void y() {
		ubicacion.setY(20);
		assertEquals(20, ubicacion.getY().intValue());
	}

	@Test
	public void diametro() {
		ubicacion.setDiametro(30);
		assertEquals(30, ubicacion.getDiametro().intValue());
	}

}