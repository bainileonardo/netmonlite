

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.netmon.model.Dispositivo;
import com.netmon.model.Prueba;
import com.netmon.model.Ubicacion;

public class DispositivoTest {

	Dispositivo dispositivo = new Dispositivo("disp1", "127.0.0.1", "True");
	List<Prueba> pruebas = new ArrayList<>();
	Prueba ping = new Prueba("Ping", "127.0.0.1", "2000");

	@Test
	public void Dispositivo() {
		assertNotNull(dispositivo);
		assertNotNull(new Dispositivo());
	}

	@Test
	public void nombre() {
		dispositivo.setNombre("disp2");
		assertTrue(dispositivo.getNombre().equals("disp2"));
	}

	@Test
	public void ip() {
		dispositivo.setIp("8.8.8.8");
		assertTrue(dispositivo.getIp().equals("8.8.8.8"));
	}

	@Test
	public void estado() {
		dispositivo.setEstado("Alerta");
		assertTrue(dispositivo.getEstado().equals("Alerta"));
	}

	@Test
	public void ubicacion() {
		dispositivo.setUbicacion(new Ubicacion());
		assertNotNull(dispositivo.getUbicacion());
	}

	@Test
	public void pruebas() {
		dispositivo.setPruebas(pruebas);
		assertNotNull(dispositivo.getPruebas());
	}

	@Test
	public void actualizarEstado() {
		ping.setResultado("false");
		pruebas.add(ping);
		dispositivo.setPruebas(pruebas);
		
		dispositivo.actualizarEstado();
		assertTrue(dispositivo.getEstado().equals("Alerta"));
		
		ping.setResultado("true");
		pruebas.add(ping);
		dispositivo.setPruebas(pruebas);
		
		dispositivo.actualizarEstado();
		assertTrue(dispositivo.getEstado().equals("Normal"));
	}

}