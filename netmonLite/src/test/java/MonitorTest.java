

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;


import com.netmon.model.Dispositivo;
import com.netmon.model.Monitor;
import com.netmon.model.Prueba;


public class MonitorTest {

	

	@Test
	public void Monitor() throws InterruptedException {
		Monitor monitor=new Monitor("C:\\Users\\AR00122207\\Desktop\\", agregarDispositivo());		
		monitor.setLoopInfinito(false);
		monitor.monitorear();
		
		
	}
	@Test
	public void MonitorVacio() throws InterruptedException {
		List<Dispositivo>dispositivos=new ArrayList<Dispositivo>();
		Monitor monitor=new Monitor("C:\\Users\\AR00122207\\Desktop\\", dispositivos);		
		monitor.setLoopInfinito(false);
		monitor.monitorear();
		
		
	}

	
	
	
private static List<Dispositivo> agregarDispositivo() {
		
		Dispositivo router=new Dispositivo("Router","192.168.1.1","Desconocido");
		Dispositivo impresora=new Dispositivo("Impresora Laser Brother","192.168.1.2","Desconocido");
		Dispositivo camara=new Dispositivo("CCTV Camara trasera patio","192.168.1.3","Desconocido");
		Dispositivo sitioInternet=new Dispositivo("Internet (Google)","www.google.com.ar","Desconocido");
		
		Prueba pingGenerico=new Prueba("Ping","booleanPing.jar", "10000");		
		Prueba google443=new Prueba("Port Ckecker a Google puerto seguro 443","booleanPort.jar","443", "10000");
		Prueba google80=new Prueba("Port Ckecker a Google puerto inseguro 80","booleanPort.jar","80", "10000");
		Prueba google90=new Prueba("Port Ckecker a Google puerto que no anda","booleanPort.jar","90", "10000");
		
		List<Prueba> unaListaDePruebas=new ArrayList<Prueba>();
		List<Prueba> otraListaDEPruebasQueContieneLaPrimera=new ArrayList<Prueba>();
		
		unaListaDePruebas.add(pingGenerico);
		
		otraListaDEPruebasQueContieneLaPrimera.addAll(unaListaDePruebas);
		otraListaDEPruebasQueContieneLaPrimera.add(google443);
		otraListaDEPruebasQueContieneLaPrimera.add(google80);
		otraListaDEPruebasQueContieneLaPrimera.add(google90);
		
			
		
		router.setPruebas(unaListaDePruebas);	
		impresora.setPruebas(unaListaDePruebas);	
		camara.setPruebas(unaListaDePruebas);	
		sitioInternet.setPruebas(otraListaDEPruebasQueContieneLaPrimera);			
		
		List<Dispositivo> dispositivos=new ArrayList<Dispositivo>();
		dispositivos.add(router);		
		dispositivos.add(impresora);		
		dispositivos.add(camara);		
		dispositivos.add(sitioInternet);
		return dispositivos;
	}

}
