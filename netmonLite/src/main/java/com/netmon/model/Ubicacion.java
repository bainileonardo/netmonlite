package com.netmon.model;

public class Ubicacion {

	private Integer x;
	private Integer y;
	private Integer diametro;

	public Ubicacion() {
		this.diametro = 10;
	}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public Integer getDiametro() {
		return diametro;
	}

	public void setDiametro(Integer diametro) {
		this.diametro = diametro;
	}

}