package com.netmon.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Prueba {

	private String nombre;
	private String frecuencia;
	private String resultado;
	private boolean ejecucion;
	private String parametros;
	private String ubicacionEjecutable;
	private String nombreEjecutable;

	public Prueba(String nombre, String nombreEjecutable,String frecuencia) {
		this.nombre = nombre;
		this.frecuencia = frecuencia;
		this.resultado = "";
		this.ejecucion = false;
		this.parametros = "";
		this.nombreEjecutable=nombreEjecutable;
	}
	public Prueba(String nombre, String nombreEjecutable,String parametros,String frecuencia) {
		this.nombre = nombre;
		this.frecuencia = frecuencia;
		this.resultado = "";
		this.ejecucion = false;
		this.parametros = parametros;
		this.nombreEjecutable=nombreEjecutable;
	}

	public Prueba() {
		super();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	

	public String getFrecuencia() {
		return frecuencia;
	}

	public void setFrecuencia(String frecuencia) {
		this.frecuencia = frecuencia;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public boolean isEjecucion() {
		return ejecucion;
	}

	public void setEjecucion(boolean ejecucion) {
		this.ejecucion = ejecucion;
	}

	public String getParametros() {
		return parametros;
	}

	public void setParametros(String parametros) {
		this.parametros = parametros;
	}

	/**
	 * La prueba se ejecuta, y guarda su resultado, luego el dispositivo sabr� en que estado ponerse
	 * con los resultados de sus pruebas.
	 * @param pathPruebas
	 * @param disp
	 * @throws InterruptedException 
	 */
		
	public String ejecutarPrueba(String pathPruebas,Dispositivo disp) throws InterruptedException {
		
		Process proceso;
	
		String comando="java -jar " + pathPruebas +nombreEjecutable +" "+ disp.getIp() +" "+parametros;
		try {
			proceso=Runtime.getRuntime().exec(comando);
			proceso.waitFor();
		    BufferedReader br = new BufferedReader(new InputStreamReader(proceso.getInputStream())); //Ya tenemos el "lector"
			resultado=br.readLine(); // Leemos la linea que sali� por consola
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return resultado;
	}
	
	public String getUbicacionEjecutable() {
		return ubicacionEjecutable;
	}

	public void setUbicacionEjecutable(String ubicacionEjecutable) {
		this.ubicacionEjecutable = ubicacionEjecutable;
	}

	public String getNombreEjecutable() {
		return nombreEjecutable;
	}

	public void setNombreEjecutable(String nombreEjecutable) {
		this.nombreEjecutable = nombreEjecutable;
	}

}