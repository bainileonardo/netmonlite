package com.netmon.model;

import java.util.List;

public class Dispositivo {
	

	private String nombre;
	private String ip;
	private String estado;
	private Ubicacion ubicacion;
	private List<Prueba> pruebas;

	public Dispositivo(String nombre, String ip, String estado) {
		this.nombre = nombre;
		this.ip = ip;
		this.estado = estado;
	}

	public Dispositivo() {
		super();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Ubicacion getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(Ubicacion ubicacion) {
		this.ubicacion = ubicacion;
	}

	public List<Prueba> getPruebas() {
		return pruebas;
	}

	public void setPruebas(List<Prueba> pruebas) {
		this.pruebas = pruebas;
	}

	/**
	 * Si alguna de las pruebas di� false, entonces estado alerta
	 * Esto permite saber y listar las pruebas en false.
	 */
	public void actualizarEstado() {
		
		for (Prueba prueba : getPruebas()) {
			String pruebaFallida=null;
			if(prueba.getResultado()!=null) {
			if (prueba.getResultado().equalsIgnoreCase("False")) {
				pruebaFallida=prueba.getResultado();				
			} 
			if(pruebaFallida!=null) {
				setEstado("Alerta");		
				
			}else {
				setEstado("Normal");
			}
			}
			setEstado("Desconocido");
			
		}
		listarPruebasPorEstado("True");
		listarPruebasPorEstado("False");
		
	}
	public void listarPruebasPorEstado(String estado) {
		Prueba prueba;
		for(int i=0;i<pruebas.size();i++) {
			prueba=pruebas.get(i);
			
			if(prueba.getResultado()!=null) {
			if(prueba.getResultado().equalsIgnoreCase(estado)) {
				if(estado.equalsIgnoreCase("False")) {
				//System.out.println("*********************************************************************************************************************************************");
				System.out.println("******Prueba fallida "+prueba.getNombre()+" usando "+prueba.getNombreEjecutable()+" a la ip "+this.getIp()+" "+prueba.getParametros()+"******");
				//System.out.println("*********************************************************************************************************************************************");
				
				}
				else {
				System.out.println("Prueba Ok "+prueba.getNombre()+" usando "+prueba.getNombreEjecutable()+" a la ip "+this.getIp()+" "+prueba.getParametros());
				
				}
				
			}
			}else {
				System.out.println("ERROR! LA PRUEBA "+prueba.getNombre()+" usando "+prueba.getNombreEjecutable()+" a la ip "+this.getIp()+" "+prueba.getParametros()+"\n NO DEVOLVI� NI TRUE NI FALSE"
						+ " REVISA EL FUNCIONAMIENTO DE LA HERRAMIENTA EXTERNA "+prueba.getNombreEjecutable() );
			}
				
		}
	}
	

}