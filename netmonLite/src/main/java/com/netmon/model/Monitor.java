package com.netmon.model;

import java.util.List;

public class Monitor implements Runnable {

	
	private String pathPruebas;
	private List<Dispositivo> dispositivos;
	private int tiempoEspera=10000;
	private boolean loopInfinito=true;
	

	public Monitor(String pathPruebas, List<Dispositivo> dispositivos) {
		
		this.pathPruebas = pathPruebas;
		this.dispositivos = dispositivos;
		
	}
	

	@Override
	public void run() {
		try {
			monitorear();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Recorre un array de dispositivos con el objetivo de invocar a las pruebas que tienen
	 * adentro, y as� el and de las pruebas, dar� estado de alerta o normal en el dispositivo
	 * cambiando su estado a normal, alerta.
	 * @throws InterruptedException 
	 */
	public void monitorear() throws InterruptedException  {
		if (!dispositivos.isEmpty()) {
			for (int i = 0; i < dispositivos.size(); i++) {
				ejecutarPruebas(dispositivos.get(i));
				
			}
		} else {
			System.out.println("No hay dispositivos que monitorear");
			try {
			System.out.println("Esperando "+tiempoEspera/1000+" segundos para reintentar escaneo...");
				Thread.sleep(tiempoEspera);
			
			} catch (InterruptedException e) {				
				e.printStackTrace();
			}
		}
		System.gc(); // Llamando al Garbage collector para liberar memoria antes de entrar de nuevo
		if(loopInfinito)monitorear();
	}
	/**
	 * 
	 * Recorre las pruebas que tenga un dispositivo y les ordena ejecutarse
	 * llamando al m�todo ejecutar prueba de la clase prueba.
	 * @throws InterruptedException 
	 */
	private void ejecutarPruebas(Dispositivo dispositivo) throws InterruptedException {
		for (Prueba prueba : dispositivo.getPruebas()) {			
				prueba.ejecutarPrueba(pathPruebas, dispositivo);						
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {			
			e.printStackTrace();
		}
		dispositivo.actualizarEstado();
	}


	public boolean isLoopInfinito() {
		return loopInfinito;
	}


	public void setLoopInfinito(boolean loopInfinito) {
		this.loopInfinito = loopInfinito;
	}
	

}